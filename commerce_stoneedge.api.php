<?php

/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * Allows modules to alter product values used in Stone Edge downloadorders output.
 *
 * The Stone Edge basic/required parameters are included by default in each product.
 * This hook allows a developer to modify the product line item values, or add
 * new values. The most common usage would be to include the Size or Color options
 * of the product.
 *
 * The $product_wrapper and $line_item_wrapper params are entity_metadata_wrapper
 * objects. Entity API is required for commerce_stoneedge for other things, so it
 * makes sense to use it. Use $product_wrapper->value() if you are uncomfortable
 * with entity_metadata_wrappers.
 *
 * @param array $product
 *   Alterable product info for Stone Edge
 * @param object $product_wrapper
 *   An entity_metatdata_wrapper for the original product
 * @param object $line_item_wrapper
 *   An entity_metatdata_wrapper for the order line item
 */
function hook_commerce_stoneedge_do_product_alter(&$product, $product_wrapper, $line_item_wrapper) {
  //Example: Add information about the size/color variation of the product.
  $opts = array();

  if ($product_wrapper->field_color->value()) {
    $opts['Color'] = $product_wrapper->field_color->label();
  }

  if ($product_wrapper->field_size->value()) {
    $opts['Size'] = $product_wrapper->field_size->label();
  }

  // Stone Edge uses <OrderOption> for each option in XML.
  // commerce_stonedge will turn each "OrderOption__foo" into
  // an <OrderOption> when it generates the XML output.
  foreach ($opts as $key => $value) {
    $product['OrderOption__' . $key] = array(
      'OptionName' => $key,
      'SelectedOption' => $value,
    );
  }
}

/**
 * Allows modules to alter order values used in Stone Edge downloadorders output.
 *
 * This hook allows a developer to modify orders after they have been processed by
 * commerce_stoneedge, but before the final XML is generated.
 *
 * The $order_wrapper parameter is an entity_metadata_wrapper.
 * Entity API is required for commerce_stoneedge for other things, so it
 * makes sense to use it. Use $order_wrapper->value() if you are uncomfortable
 * with entity_metadata_wrappers.
 *
 * @param array $processed_order
 *   Alterable order info for Stone Edge
 * @param object $order_wrapper
 *   An entity_metatdata_wrapper for the original product
 */
function hook_commerce_stoneedge_do_order_alter(&$processed_order, $order_wrapper) {
  //Example: Update the billing phone number to use a specific phone number field
  if (isset($order_wrapper->commerce_customer_billing->field_myphonenumber)) {
    $processed_order['Billing']['Phone'] = $order_wrapper->commerce_customer_billing->field_myphonenumber->value();
  }
}