GENERAL
-------------
This module works with the "Generic" shopping cart feature of Monsoon Stone Edge.
The module provides inventory and order synchronization between Stone Edge and Drupal
7 / Drupal Commerce.

Currently the module supports:
1) Live inventory updates - to keep stock values in Drupal updated as QOH changes within Stone Edge.
2) QOH Replace - to send stock information for all products from Stone Edge to Drupal.
3) Download orders - to retrieve Drupal orders in Stone Edge.

REQUIREMENTS
-------------
1) Any of the modules listed as dependencies in commerce_stoneedge.info
2) Monsoon Stone Edge + Generic Shopping Cart license
   * You must have a license that allows you to use the "Generic" shopping cart.
     At the time of writing this was a $499 purchase.
3) HTTPS support
   * Your website must support HTTPS traffic, at least at the following URL
   https://YOURSITE.COM/commerce-stoneedge/api

INSTALLATION
-------------
1) Enable the module
2) Create a user account that has access to 'edit any commerce_product entity.'
3) Configure your generic shopping cart in Stone Edge
   * URL should be YOURSITE.COM/commerce-stoneedge/api
   * Use credentials for your new account
   * Enable inventory synchronization
   * Set the GenericImportMethod system parameter to 'XML'

CONFIGURATION
-------------
None really... it just works ;)

The fields included in the downloadorders functionality are based on the minimum required by StoneEdge.
The values for these fields are taken from the typical Drupal Commerce setup. If you need to add/edit/remove
any fields from the response, you can do so through a few API functions.
See commerce_stoneedge.api.php.

TESTING / USAGE
---------------
Live Inventory: make an inventory change in StoneEdge and watch the stack controller.
	Make sure you don't see any errors in the stack controller. If there are problems
	I suggest using devel to show the contents of $_POST within the
	commerce_stoneedge_api_request() function. The code would look like:

	function commerce_stoneedge_api_request() {
	  dd($_POST);
	  ...

QOH Replace: Use the button in stone edge to Send QOH to the shopping cart.
